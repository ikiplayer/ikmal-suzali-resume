export default ({ app }, inject) => {
  // SNIPPET FROM DEFAULT.VUE - IKMAL SUZALI PERSONAL WEBSITE
  inject('snippetResumeDefaultFile', () => {
    return `
<template>
<!-- THIS IS AN OVERLAY FOR THE ACTION TAB BAR FOR ALL PAGES -->
  <v-app>
    <v-main>
      <v-container class="">
        <nuxt :key="$nuxt.$route.path" />
        <div class="v-tab-bar">
          <div v-for="navItem in navItems" class="self-center">
            <nuxt-link
              class="v-tab-bar-item"
              :class="{ 'with-label': navItem.label }"
              :to="navItem.to"
            >
              <img
                :src="navItem.image"
                style="height: 20px; margin-top: 5px; margin: auto"
              />
              <div class="v-tab-bar-item-label mt-1" v-if="navItem.label">
                {{ navItem.label }}
              </div>
            </nuxt-link>
          </div>
        </div>
      </v-container>
    </v-main>
  </v-app>
</template>

<script>
export default {
  data() {
    return {
      navItems: [
        {
          label: 'Home',
          to: '/home',
          image: '/icons8-home-50.png',
        },
        {
          label: 'Work',
          to: '/work/work-experience',
          image: '/icons8-video-50.png',
        },
        {
          label: 'Chat',
          to: '/maintab3',
          image: '/icons8-delete-message-50.png',
        },
        {
          label: 'About Me',
          to: '/maintab4',
          image: '/icons8-eye-50.png',
        },
      ],
    }
  },
  methods: {},
  created() {
  },
}
</script>

<style lang="css" scoped>
.v-tab-bar {
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  background: #eee;
  display: flex;
  height: 45px;
  justify-content: space-around;
  z-index: 9999;
}

@media screen and (min-width: 800px) {
  .v-tab-bar {
    padding-left: 120px;
    padding-right: 120px;
  }
}

.v-tab-bar-router-view {
  margin-bottom: 45px;
}

.v-tab-bar .v-tab-bar-item {
  padding-top: 8px;
  padding-right: 5px;
  padding-left: 5px;
  padding-bottom: 5px;
  flex: 1 0 0;
  text-align: center;
  color: #777;
  text-decoration: none;
}
.v-tab-bar .v-tab-bar-item.with-label {
  padding-top: 5px;
}
.v-tab-bar .v-tab-bar-item.with-label i {
  font-size: 1.6em;
}
.v-tab-bar .v-tab-bar-item i {
  font-size: 2em;
}
.v-tab-bar .v-tab-bar-item i.inactive {
  display: block;
}
.v-tab-bar .v-tab-bar-item i.active {
  display: none;
}
.v-tab-bar .v-tab-bar-item.router-link-active {
  color: #337ab7;
}
.v-tab-bar .v-tab-bar-item.router-link-active i.inactive {
  display: none;
}
.v-tab-bar .v-tab-bar-item.router-link-active i.active {
  display: block;
}
.v-tab-bar .v-tab-bar-item .v-tab-bar-item-label {
  font-size: 10px;
  line-height: 10px;
}
.v-tab--active {
  font-size: 19px;
}
</style>
`
  })

  // SNIPPET FROM CHAT CONTROLLER API - TEXTYTALES.COM
  inject('snippetTextyTalesControllerApiFile', () => {
    return `
// THIS IS A SNIPPET MAKING AN API CALL TO USING A AGGREGATE QUERY WITH MONGODB
exports.chatList = [
  authBypass,
  async (req, res) => {
    try {
      let mainTagQuery = req.query.main_tag;
      let searchQuery = req.query.search;
      let tagQuery = req.query.tag;
      let viewTypeQuery = req.query.view_type;
      let isRandom = req.query.is_random;
      let skip = req.query.skip ? req.query.skip : 0;
      let maxPerPage = req.query.max_per_page ? req.query.max_per_page : 20;

      let aggregateQuery = [
        {
          $match: {
            is_private: false,
          },
        },
        {
          $lookup: {
            from: "messages",
            localField: "messages",
            foreignField: "_id",
            as: "messages",
          },
        },
        {
          $lookup: {
            from: "chatviews",
            localField: "_id",
            foreignField: "chat",
            as: "views",
          },
        },
        {
          $lookup: {
            from: "userchatlikes",
            localField: "_id",
            foreignField: "chat",
            as: "likes",
          },
        },
        {
          $lookup: {
            from: "uploads",
            localField: "main_image",
            foreignField: "_id",
            as: "main_image",
          },
        },
        {
          $addFields: {
            total_views: { $size: "$views" },
            total_likes: {
              $size: {
                $filter: {
                  input: "$likes",
                  as: "likes",
                  cond: { $eq: ["$$likes.is_liked", true] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            daily_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", new Date("2010-01-01")] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            monthly_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", new Date()] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            yearly_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", new Date()] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            is_liked: {
              // $size: {
              $filter: {
                input: "$likes",
                as: "likes",
                cond: [{ $eq: ["$$likes.user", "$user"] }, []],
              },
              // }
            },
          },
        },
        {
          $unwind: {
            path: "$is_liked",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $unwind: {
            path: "$main_image",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $addFields: {
            is_liked: {
              $cond: {
                if: { $eq: ["$is_liked.is_liked", true] },
                then: true,
                else: false,
              },
            },
          },
        },
        {
          $unset: ["views", "likes", "fake_users"],
        },
        {
          $skip: Number(skip),
        },
        {
          $limit: Number(maxPerPage),
        },
      ];

      if (viewTypeQuery != "my_chats") {
        aggregateQuery.unshift({
          $match: {
            is_private: false,
          },
        });
      }

      // VIEW TYPE
      switch (viewTypeQuery) {
        case "daily":
          aggregateQuery.push({
            $sort: {
              daily_views: -1,
            },
          });
          break;
        case "weekly":
          aggregateQuery.push({
            $sort: {
              weekly_views: -1,
            },
          });
          break;
        case "monthly":
          aggregateQuery.push({
            $sort: {
              monthly_views: -1,
            },
          });
          break;
        case "yearly":
          aggregateQuery.push({
            $sort: {
              yearly_views: -1,
            },
          });
          break;
        case "recent":
          aggregateQuery.push({
            $sort: {
              createdAt: -1,
            },
          });
          break;
        case "my_likes":
          aggregateQuery.push({
            $match: {
              is_liked: true,
            },
          });
          break;
        case "my_chats":
          aggregateQuery.push({
            $match: {
              user: ObjectId('$req.user._id'),
            },
          });
          aggregateQuery.shift();
          break;
        case "random":
          aggregateQuery.push({
            $sample: { size: Number(maxPerPage) },
          });
          break;
        default:
          break;
      }

      // TAG FILTER

      if (mainTagQuery) {
        let tags = [];
        tags = await Tag.find({ main_tag: mainTagQuery }, { _id: 1 });

        tags = tags.map((tag) => {
          return ObjectId(tag._id);
        });
        aggregateQuery.push({
          $match: {
            tags: { $in: tags },
          },
        });
      }

      if (searchQuery) {
        aggregateQuery.unshift({
          $sort: { score: { $meta: "textScore" } },
        });
        aggregateQuery.unshift({
          $match: { $text: { $search: searchQuery } },
        });
      }

      let chats = await Chat.aggregate(aggregateQuery);

      if (chats.length > 0) {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          chats
        );
      } else {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          []
        );
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

`
  })

  // SNIPPET FROM CHAT WEB - TEXTYTALES.COM

  inject('snippetTextyTalesChatWebFile', () => {
    return `
<template>
  <div id="chat-app" class="border border-solid d-theme-border-grey-light rounded relative overflow-hidden">
    <div id="parentx-demo-2">
      <vs-sidebar
        parent="#chat-app"
        :position-right="true"
        :hidden-background="true"
        v-model="isProfileSidebarOpen"
        :click-not-close="true"
        id="chat-profile-sidebar"
        class="items-no-padding"
      >
        <div class="header-sidebar relative flex flex-col p-0" slot="header">
          <feather-icon
            icon="XIcon"
            svgClasses="m-2 cursor-pointer absolute top-0 right-0"
            @click="isProfileSidebarOpen = false"
          ></feather-icon>

          <div class="relative inline-flex mx-auto mb-5 mt-6">
            <vs-avatar
              @click="onFileUpload('chat-main')"
              class="m-0 border-white border-2 border-solid shadow-md"
              :src="getValue(chat.chat_image, 'url')"
              size="70px"
            />
          </div>
          <vs-input placeholder="Title" v-model="chat.title" class="w-full mx-10 mx-auto" />
          <div class="mt-4 flex">
            <div class="mr-5">Chat Privacy</div>
            <vs-switch color="success" style="width: 75px" v-model="chat.is_private">
              <span slot="on">Private</span>
              <span slot="off">Public</span>
            </vs-switch>
          </div>
          <div class="mt-4">
            Chat Theme
          </div>
          <div class="mt-4 flex">
            <div @click="onChatThemeClick(0)" class="mx-3" :class="[{item_selected: chat.chat_layout_type == 0}]">
              <vs-button color="primary" type="border" style="padding: 5px 12px;">
                <img class="h-6" src="../../../assets/images/misc/icons8-whatsapp-30.png" alt="" />
              </vs-button>
            </div>
            <div @click="onChatThemeClick(1)" class="mx-3" :class="[{item_selected: chat.chat_layout_type == 1}]">
              <vs-button color="primary" type="border" style="padding: 5px 12px;">
                <img class="h-6" src="../../../assets/images/misc/icons8-tinder-30.png" alt="" />
              </vs-button>
            </div>
            <div @click="onChatThemeClick(2)" class="mx-3" :class="[{item_selected: chat.chat_layout_type == 2}]">
              <vs-button color="primary" type="border" style="padding: 5px 12px;">
                <img class="h-6" src="../../../assets/images/misc/icons8-messages-48.png" alt="" />
              </vs-button>
            </div>
          </div>
          <div class="my-4">
            <div>
              Tags
            </div>
            <vs-select placeholder="Add up to 5 tags" class="w-full" multiple autocomplete v-model="chat.tags">
              <vs-select-item v-for="(tag, index) in tagItems" :key="index" :value="tag._id" :text="tag.name" />
            </vs-select>
          </div>
        </div>
 

        <component :is="scrollbarTag" class="scroll-area" :settings="settings">
          <vs-list>
            <div v-for="(fakeUser, fakeUserIndex) in chat.fake_users" :key="fakeUserIndex">
              <div class="w-full max-w-lg overflow-hidden rounded-lg shadow-lg sm:flex items-center">
                <div class="flex">
                  <vs-avatar
                    :src="getValue(fakeUser.image, 'url')"
                    class="mx-2"
                    style="width: 50px; height: 50px"
                    @click="onFileUpload('fake-user', fakeUserIndex)"
                  />
                </div>
                <div class="flex-1 py-4 mx-2">
                  <input placeholder="Name" v-model="fakeUser.name" class="w-full h-10 standard-input" />
                  <input placeholder="Description" v-model="fakeUser.description" class="w-full my-2 standard-input h-6" />
                </div>
                <div class="flex py-4 m-2">
                  <vs-button icon="delete" color="danger" @click.native="onDeleteNewFakeUser(fakeUserIndex)"></vs-button>
                </div>
              </div>
            </div>
          </vs-list>
          <div>
            <vs-button
              v-if="chat.fake_users && chat.fake_users && chat.fake_users.length < 5"
              color="primary"
              type="gradient"
              class="w-11/12 mx-auto mt-6 relative flex"
              @click.native="onAddNewFakeUser"
            >
              Add new user to chat
            </vs-button>
          </div>
        </component>
      </vs-sidebar>
    </div>
    <div
      class=" no-scroll-content chat-content-area border d-theme-border-grey-light border-t-0 border-r-0 border-b-0"
      :class="[
        {chat__bg_0: chat.chat_layout_type == 0},
        {chat__bg_1: chat.chat_layout_type == 1},
        {chat__bg_3: chat.chat_layout_type == 2}
      ]"
    >
      <template>
        <div class="chat__navbar">
          <vs-navbar
            :class="[
              {chat_navbar_0: chat.chat_layout_type == 0},
              {chat_navbar_1: chat.chat_layout_type == 1},
              {chat_navbar_3: chat.chat_layout_type == 2},
              'flex',
              'navbar-custom'
            ]"
            style="padding: 1.7rem"
            type="flat"
          >
            <div class="relative flex mr-4">
              <feather-icon
                icon="SettingsIcon"
                class="mr-4 cursor-pointer"
                :class="[{chat_navbar_icon_0: chat.chat_layout_type == 0}]"
                @click="isProfileSidebarOpen = true"
              />
              <vs-avatar
                class="m-0 border-2 border-solid border-white"
                size="40px"
                :src="getValue(chat.chat_image, 'url')"
                @click="onFileUpload('chat-main')"
              />
              <!-- <div
                class="h-3 w-3 border-white border border-solid rounded-full absolute right-0 bottom-0"
                :class="'bg-success'"
                @click=""
              ></div> -->
            </div>
            <vs-input placeholder="Title" v-model="chat.title" class="w-4/12" />
            <vs-spacer></vs-spacer>
            <feather-icon
              icon="ReloadIcon"
              class="cursor-pointer mx-2"
              :svgClasses="['w-8', 'h-8']"
              @click="toggleVisibilityButton"
            ></feather-icon>
            <feather-icon
              v-if="!isLoading"
              :class="[{chat_navbar_icon_0: chat.chat_layout_type == 0}]"
              icon="SaveIcon"
              class="cursor-pointer mx-2"
              :svgClasses="['w-8', 'h-8']"
              @click="onSaveClick"
            ></feather-icon>
            <vue-loading v-else type="bars" color="white" :size="{width: '40px', height: '40px'}"></vue-loading>
          </vs-navbar>
        </div>

        <component
          :is="scrollbarTag"
          class="chat-content-scroll-area border d-theme-border-grey-light"
          :settings="settings"
          ref="chatLogPS"
        >
          <div class="chat__log" ref="chatLog">
            <div id="component-chat-log" class="m-8">
              <draggable
                :list="chat.messages"
                handle=".handle"
                animation="500"
                easing="cubic-bezier(1, 0, 0, 1)"
                :force-fallback="true"
                ghost-class="ghost"
                :scroll-sensitivity="400"
              >
                <div v-for="(message, messageIndex) in chat.messages" class="msg-grp-container" :key="messageIndex">
                  <template v-if="message[messageIndex - 1]">
                    <vs-divider v-if="!isSameDay(message.time, message[messageIndex - 1].time)" class="msg-time">
                      <span>{{ toDate(message.time) }}</span>
                    </vs-divider>
                    <div class="spacer mt-8" v-if="!hasSentPreviousMsg(message[messageIndex - 1].message_type, message.message_type)"></div>
                  </template>

                  <div class="flex items-start" :class="[{'flex-row-reverse': message.message_type}]">
                    <template>
                      <vs-dropdown vs-custom-content vs-trigger-click>
                        <vs-avatar
                          :src="getValue(message.fake_user, 'image.url')"
                          v-if="!message.message_type && chat.fake_users && chat.fake_users.length >= 1"
                          size="40px"
                          class="border-2 shadow border-solid border-white m-0 flex-shrink-0 my-5"
                          :class="message.message_type ? 'sm:ml-5 ml-3' : 'sm:mr-5 mr-3'"
                        ></vs-avatar>
                        <vs-dropdown-menu class="loginx">
                          <vs-list class="p-0">
                            <div
                              v-for="(fakeUser, fakeUserIndex) in chat.fake_users"
                              :key="fakeUserIndex"
                              class="my-1"
                              @click="onSelectFakeUser(messageIndex, fakeUser)"
                            >
                              <div class="w-full max-w-lg overflow-hidden rounded-lg border-solid border-2 sm:flex items-center">
                                <div class="flex">
                                  <vs-avatar :src="getValue(fakeUser.image, 'url')" class="mx-2" style="width: 20px; height: 20px" />
                                </div>
                                <div class="flex-1 py-2 mx-2">
                                  <div>{{ fakeUser.name }}</div>
                                </div>
                              </div>
                            </div>
                          </vs-list>
                        </vs-dropdown-menu>
                      </vs-dropdown>
                    </template>

                    <template v-if="message[messageIndex - 1]">
                      <div
                        class="mr-16"
                        v-if="
                          !(
                            !hasSentPreviousMsg(message[messageIndex - 1].message_type, message.message_type) ||
                            !isSameDay(message.time, message[messageIndex - 1].time)
                          )
                        "
                      ></div>
                    </template>
                    <vs-card
                      style="margin-top:15px"
                      class="cardx msg break-words relative shadow-md rounded px-4 mb-2 rounded-lg max-w-md"
                      :class="[
                        {
                          chat_mychat_0: message.message_type && chat.chat_layout_type == 0
                        },
                        {
                          chat_mychat_1: message.message_type && chat.chat_layout_type == 1
                        },
                        {
                          chat_mychat_3: message.message_type && chat.chat_layout_type == 2
                        },
                        {
                          chat_theirchat_0: !message.message_type && chat.chat_layout_type == 0
                        },
                        {
                          chat_theirchat_1: !message.message_type && chat.chat_layout_type == 1
                        },
                        {
                          chat_theirchat_3: !message.message_type && chat.chat_layout_type == 2
                        }
                      ]"
                    >
                      <div slot="header">
                        <img v-if="getValue(message.image, 'url')" width="100%" class="p-5" :src="message.image.url" />

                        <textarea-autosize
                          v-else
                          class="text-base border-none"
                          :class="[
                            {
                              'text-white':
                                (message.message_type && chat.chat_layout_type !== 0) ||
                                (!message.message_type && chat.chat_layout_type == 2)
                            },
                            {
                              'text-black': message.message_type && chat.chat_layout_type == 0
                            }
                          ]"
                          style="font-weight: 200; font-family: 'Montserrat', Helvetica, Arial, sans-serif; background-color: transparent; width: 100%"
                          placeholder="Add message"
                          ref="myTextarea"
                          v-model="message.message"
                          @focus.native="onMessageClick(messageIndex)"
                        />
                      </div>
                      <div slot="footer">
                        <vs-row vs-justify="flex-end">
                          <vs-button
                            v-if="!message.message_type"
                            color="primary"
                            type="gradient"
                            class="mx-2"
                            icon="redo"
                            @click="message.message_type = !message.message_type"
                          >
                            Me
                          </vs-button>
                          <vs-button
                            v-if="message.message_type"
                            color="primary"
                            type="gradient"
                            class="mx-2"
                            icon="undo"
                            @click="message.message_type = !message.message_type"
                          >
                            Them
                          </vs-button>
                          <vs-button color="primary" type="gradient" icon="drag_handle" class="handle mx-2"></vs-button>
                          <vs-button color="danger" type="gradient" icon="delete" @click="onMessageDeleteClick(messageIndex)"> </vs-button>
                        </vs-row>
                      </div>
                    </vs-card>
                  </div>
                </div>
              </draggable>
            </div>
          </div>
        </component>
        <div class="chat__input flex p-4 bg-white">
          <vs-input class="flex-1 mr-3" placeholder="Type Your Message" v-model="newMessage" @keyup.enter="sendMsg" />
          <vs-button color="primary" type="gradient" class="mx-2" icon="undo" @click="onThemMeClickNewMessageClick('Them')">
            Them
          </vs-button>
          <vs-button color="primary" type="gradient" class="mx-2" icon="redo" @click="onThemMeClickNewMessageClick('Me')">
            Me
          </vs-button>
          <vs-button color="primary" type="gradient" icon="attachment" class="mx-2" @click="onFileUpload('chat-message')"></vs-button>
        </div>
      </template>
    </div>
    <vs-popup class="Write Message" title="Chat Message" :active.sync="messagePopupActive">
      <vs-textarea style="min-height:120px" v-model="chatMessage" />
      <vs-button color="primary" type="filled" class="mb-4 block mx-auto w-full" @click="onUpdateMessageClick()">
        Update
      </vs-button>
      <picker style="width:100%" title="Texty Tales" @select="onEmojiClick($event)" />
    </vs-popup>
    <file-upload
      class="btn btn-primary w-full"
      :drop="true"
      accept="image/*"
      @input-file="inputImageFile"
      @input-filter="inputFilter"
      input-id="myuploader"
    >
    </file-upload>
  </div>
</template>

<script>
import {VueLoading} from "vue-loading-template";
import ChatContact from "./ChatContact.vue";
import ChatLog from "./ChatLog.vue";
import ChatNavbar from "./ChatNavbar.vue";
import UserProfile from "./UserProfile.vue";
import VuePerfectScrollbar from "vue-perfect-scrollbar";
import ChatItem from "./ChatItem";
import get from "get-value";
import draggable from "vuedraggable";
import {TwemojiPicker} from "@kevinfaguiar/vue-twemoji-picker";
import EmojiAllData from "@kevinfaguiar/vue-twemoji-picker/emoji-data/en/emoji-all-groups.json";
import EmojiDataAnimalsNature from "@kevinfaguiar/vue-twemoji-picker/emoji-data/en/emoji-group-animals-nature.json";
import EmojiDataFoodDrink from "@kevinfaguiar/vue-twemoji-picker/emoji-data/en/emoji-group-food-drink.json";
import EmojiGroups from "@kevinfaguiar/vue-twemoji-picker/emoji-data/emoji-groups.json";
import {TwemojiTextarea} from "@kevinfaguiar/vue-twemoji-picker";
import {Picker} from "emoji-mart-vue";
import api from "../../../axios";

export default {
  data() {
    return {
      chatId: this.$route.params.chatId,
      hasSound: true,
      chat: {
        chat_image: {},
        messages: [],
        fake_users: [],
        tags: [],
        description: "",
        title: "",
        chat_type: 1,
        is_private: false,
        chat_layout_type: 0
      },
      puppeteerSecret: this.$route.query.access,
      uploadType: "",
      messageIndex: null,
      chatMessage: "",
      messagePopupActive: false,
      newMessage: "",
      visibleMessageIndex: 0,
      isProfileSidebarOpen: false,
      messagePlayStart: 0,
      isFavoriteChat: false,
      isPlaying: true,
      isVisible: false,
      active: true,
      isHidden: false,
      searchContact: "",
      activeProfileSidebar: true,
      activeChatUser: {},
      userProfileId: -1,
      typedMessage: "",
      isChatPinned: false,
      settings: {
        maxScrollbarLength: 60,
        wheelSpeed: 0.7
      },
      tagItems: [],
      inputType: null,
      inputIndex: 0,
      clickNotClose: true,
      isChatSidebarActive: false,
      isLoggedInUserProfileView: false,
      relatedChats: [],
      isLoading: false
    };
  },
  watch: {
    windowWidth() {
      this.setSidebarWidth();
    },
    isPlaying(newValue) {
      if (newValue == false) {
        clearInterval(this.interval);
      } else {
        this.interval = setInterval(() => {
          this.visibleMessageIndex++;
        }, 1500);
      }
    },
    visibleMessageIndex(newValue, oldValue) {
      this.chat.messages.forEach((item, index) => {
        if (index <= newValue) {
          this.chat.messages[index].is_visible = true;
        } else {
          this.chat.messages[index].is_visible = false;
        }
      });
    }
  },
  computed: {
    getAvatarImage() {
      if (this.getValue(this.chat.fake_users[0], "image.url")) {
        return this.getValue(this.chat.fake_users[0], "image.url");
      }
    },
    statusColor() {
   

      return "success";
    },
    getStatusColor() {
      return "success";
    },

    scrollbarTag() {
      return this.$store.getters.scrollbarTag;
    },

    windowWidth() {
      return this.$store.state.windowWidth;
    },
    emojiDataAll() {
      return EmojiAllData;
    },
    emojiGroups() {
      return EmojiGroups;
    }
  },
  props: {
    itemSelected: {
      type: Object,
      default: () => {}
    }
  },

  methods: {
    getValue(resource, key) {
      return get(resource, key);
    },
    onChatThemeClick(chatThemeType) {
      this.chat.chat_layout_type = chatThemeType;

      switch (chatThemeType) {
        case 0:
          break;
        case 1:
          break;
        case 2:
        default:
          break;
      }
    },
    onFileUpload(inputType, index = null) {
      this.inputType = inputType;
      this.inputIndex = index;
      document.getElementById("myuploader").click();
    },
    onSelectFakeUser(messageIndex, fakeUser) {

      this.$set(this.chat.messages[messageIndex], "fake_user", fakeUser);
    },
    onNewMessageClick() {
      this.messagePopupActive = true;
      this.messageIndex = "new";
    },
    addFile(chatType) {
      this.chatType = chatType;
      let input = this.$refs.upload.$el.querySelector("input");
      input.click();
      input.onclick = e => {};
    },
    uuid4() {
      let array = new Uint8Array(16);
      crypto.getRandomValues(array);
      array[8] &= 0b00111111;
      array[8] |= 0b10000000;

      array[6] &= 0b00001111;
      array[6] |= 0b01000000;

      const pattern = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
      let idx = 0;

      return pattern.replace(/XX/g, () => array[idx++].toString(16).padStart(2, "0"));
    },
    onAddNewFakeUser() {
      this.chat.fake_users.push({
        name: 'User $this.chat.fake_users.length + 1',
        image: {},
        description: "",
        temp_uuid: this.uuid4()
      });
    },
    onDeleteNewFakeUser(index) {
      let fakeUser = this.chat.fake_users[index];
      this.chat.fake_users.splice(index, 1);
      this.chat.messages.map((item, index) => {
        if (this.getValue(item.fake_user, "image.id") == fakeUser.image.id) {
          let newItem = item;
          if (this.chat.fake_users.length >= 1) {
            newItem.fake_user = this.chat.fake_users[0];
            return newItem;
          } else {
            newItem.fake_user = {};
            return newItem;
          }
        }
      });
    },
    onMessageDeleteClick(index) {
      this.chat.messages.splice(index, 1);
    },
    onUpdateMessageClick() {
      if (this.messageIndex !== "new") {
        this.chat.messages[this.messageIndex].message = this.chatMessage;
      } else {
        this.chat.messages.push(this.chatMessage);
      }
      this.messagePopupActive = false;
    },
    inputImageFile(newFile, oldFile) {
      if (newFile && !oldFile) {
        let formData = new FormData();
        formData.append("file", newFile.file);
        formData.append("type", this.inputType);
        api
          .post("upload/", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            }
          })
          .then(response => {
            if (response.data) {
              let file = response.data.data;
              if (this.inputType == "chat-main") {
                this.$set(this.chat, "chat_image", file);
              } else if (this.inputType == "fake-user" && this.inputIndex !== null) {
                this.$set(this.chat.fake_users[this.inputIndex], 'image', file);
              } else if (this.inputType == "message" && this.inputIndex !== null) {
                this.$set(this.chat.messages[this.inputIndex], "image", file);
              } else if (this.inputType == "chat-message") {
                this.chat.messages.push({
                  fake_user: {},
                  message_type: false,
                  is_visible: true,
                  image: file,
                  message: ""
                });
              }
            }
          })
          .catch(error => {})
          .finally(() => {
            this.inputType = null;
            this.inputIndex = 0;
          });


      }
    },
    getChat() {
      api
        .get("chat/" + this.chat._id)
        .then(response => {
          if (response.data) {
            this.chat = response.data.data;
            if (this.chat.chat_type == 2) {
              this.$router.replace({path: '/chat/create/2/{
                this.chat._id
              }', params: {itemSelected: response.data.data}});
            }
            if (this.chat.tags && this.chat.tags.length > 0) {
              this.chat.tags = this.chat.tags.map(tag => {
                return tag._id;
              });
            }
          }
        })
        .catch(error => {
        });
    },
    getChatTags() {
      api
        .get("tag")
        .then(response => {
          if (response.data) {
            this.tagItems = response.data.data;
          }
        })
        .catch(error => {
        });
    },
    onSaveClick() {
      this.isLoading = true;
      if (!this.chat._id) {
        api
          .post("chat/", this.chat)
          .then(response => {
            if (response.data) {
              this.chat = response.data.data;
              this.$router.replace({name: 'chat.create.type1.update', params: {chatId: this.chat._id}});
            }
          })
          .catch(error => {
            this.chatErrorMessages = this.getValue(error.response, "data.data");
          })
          .finally(() => {
            this.isLoading = false;
          });
      } else {
        api
          .put("chat/" + this.chat._id, this.chat)
          .then(response => {
            if (response.data) {
              this.chat = response.data.data;
            }
          })
          .catch(error => {
            this.chatErrorMessages = this.getValue(error.response, "data.data");
          })
          .finally(() => {
            this.isLoading = false;
          });
      }
    },
    inputFilter(newFile, oldFile, prevent) {
      if (newFile && (!oldFile || newFile.file !== oldFile.file)) {

        newFile.blob = "";
        let URL = window.URL || window.webkitURL;
        if (URL && URL.createObjectURL) {
          newFile.blob = URL.createObjectURL(newFile.file);
        }
      }
    },

    onEmojiPopupChanged() {},
    onEmojiClick(unicode) {
      this.chatMessage += unicode.native;
    },
    onMessageClick(index) {
      this.messageIndex = index;
      this.chatMessage = this.chat.messages[index].message;
      this.messagePopupActive = true;
    },
    toggleVisibilityButton() {
      this.isVisible = !this.isVisible;

      this.chat.messages.forEach(item => {
        item.is_visible = this.isVisible;
      });
    },
    toggleSoundButton() {
      return this.hasSound ? "Volume2Icon" : "VolumeXIcon";
    },
    togglePlayButton() {
      return this.isPlaying ? "PauseIcon" : "PlayIcon";
    },

    closeProfileSidebar() {
    },

    showProfileSidebar(openOnLeft = false) {
      this.isLoggedInUserProfileView = openOnLeft;
      this.activeProfileSidebar = !this.activeProfileSidebar;
    },
    isSameDay(time_to, time_from) {
      const date_time_to = new Date(Date.parse(time_to));
      const date_time_from = new Date(Date.parse(time_from));
      return (
        date_time_to.getFullYear() === date_time_from.getFullYear() &&
        date_time_to.getMonth() === date_time_from.getMonth() &&
        date_time_to.getDate() === date_time_from.getDate()
      );
    },
    toDate(time) {
      const locale = "en-us";
      const date_obj = new Date(Date.parse(time));
      const monthName = date_obj.toLocaleString(locale, {
        month: "short"
      });
      return '{date_obj.getDate()} {monthName}';
    },
    scrollToBottom() {
      this.$nextTick(() => {
        this.$parent.$el.scrollTop = this.$parent.$el.scrollHeight;
      });
    },
    onThemMeClickNewMessageClick(clickType = null) {
      this.chat.messages.push({
        fake_user: {},
        message_type: clickType == "Them" ? false : true,
        is_visible: true,
        message: this.newMessage
      });

      this.newMessage = "";
    },
    sendMsg() {
      this.chat.messages.push({
        fake_user: this.chat.fake_users.length >= 1 ? this.chat.fake_users[0] : {},
        message_type: false,
        is_visible: true,
        message: this.newMessage
      });

      this.newMessage = "";

      const scroll_el = this.$refs.chatLogPS.$el || this.$refs.chatLogPS;
      scroll_el.scrollTop = this.$refs.chatLog.scrollHeight;
    },
    toggleIsChatPinned(value) {
      this.isChatPinned = value;
    },
    setSidebarWidth() {
      if (this.windowWidth < 1200) {
        this.isChatSidebarActive = this.clickNotClose = false;
      } else {
        this.isChatSidebarActive = this.clickNotClose = true;
      }
    },
    toggleChatSidebar(value = false) {
      if (!value && this.clickNotClose) return;
      this.isChatSidebarActive = value;
    }
  },
  components: {
    VuePerfectScrollbar,
    draggable,
    "twemoji-picker": TwemojiPicker,
    "twemoji-textarea": TwemojiTextarea,
    Picker,
    VueLoading

  
  },
  updated() {
    this.scrollToBottom();
  },

  created() {
    this.setSidebarWidth();
    this.getChatTags();
    if (this.chatId) {
      this.chat._id = this.chatId;
      this.getChat();
    }
  },
  beforeDestroy() {},
  mounted() {
    this.scrollToBottom();
  }
};
</script>

<style lang="scss">
.standard-input {
  border-radius: 3px;
  border-color: gray;
  outline: none;
  border: 1px solid #ccc;
  box-sizing: border-box;
  padding-left: 8px;
}
@import "@/assets/scss/vuexy/apps/chat.scss";
textarea::placeholder {
  color: white;
}
.shift-right-enter {
  transform: translateX(500px);
  transition: all 10s cubic-bezier(1, 0.5, 0.8, 1);
}

.shift-left-enter {
  transform: translateX(500px);
  transition: all 10s cubic-bezier(1, 0.5, 0.8, 1);
}

.bounce-enter-active {
  animation: bounce-in 0.5s;
}
.bounce-leave-active {
  animation: bounce-in 0.5s reverse;
}
@keyframes bounce-in {
  0% {
    transform: scale(0);
  }
  50% {
    transform: scale(1.5);
  }
  100% {
    transform: scale(1);
  }
}

.ghost {
  opacity: 0.5;
  background: #1982fc;
}
</style>


`
  })

  //  SNIPPET FROM PERSON-LD - ONEGENERATE
  inject('snippetOneGeneratePersonFile', () => {
    return `
// THIS SNIPPET ALLOWS ANY USER TO KEY VALUES TO AUTO GENERATE THE JSON-LD FILE
import React, { Component } from 'react'
import { Row, Col, Form, Card } from 'antd'
import AceEditor from 'react-ace'
import { TextField, Toolbar, Snackbar, SnackbarContent, Button } from '@material-ui/core'
import { Helmet } from 'react-helmet'
import copy from 'copy-to-clipboard'
import 'brace/mode/php'
import 'brace/theme/vibrant_ink'

class Person extends Component {
  constructor(props) {
    super(props)
    this.state = {
      helmetTitle: 'JSON-LD Person',
      helmetDescription: 'Generate code for JSON-LD person',
      siteName: 'OneGenerate',
      isSnackbarOpen: false,
      name: '',
      jobTitle: '',
      city: '',
      stateProvinceRegion: '',
    }
  }

  componentDidMount() {
    this.updateCodeString()
  }

  updateCodeString() {
    let name = this.state.name
    let jobTitle = this.state.jobTitle
    let city = this.state.city
    let stateProvinceRegion = this.state.stateProvinceRegion

    let codeString = '
    <script type="application/ld+json">
    {
    "@context": "http://schema.org/",
    "@type": "Person",
    "name": "{name}",
    "jobTitle": "{jobTitle}",
    "address": {
        "@type": "PostalAddress",
        "addressLocality": "{city}",
        "addressRegion": "{stateProvinceRegion}"
    }
    }
    </script>
    '

    this.setState({
      codeItem: codeString,
    })
  }

  handleItem = (name, stateItem, index) => event => {
    event.persist()

    let updatedItems = this.state[name]
    updatedItems[index][stateItem] = event.target.value

    this.setState(
      {
        items: updatedItems,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  handleOnCopyClick() {
    copy(this.state.codeItem)
    this.setState({
      isSnackbarOpen: true,
    })
  }

  handleDownloadFileClick() {
    let mimeType = 'text/plain'

    var element = document.createElement('a')
    var file = new Blob([this.state.codeItem], { type: mimeType })
    element.href = URL.createObjectURL(file)
    element.download = 'JSONLD_Person.txt'
    element.click()
  }

  handleInput = name => event => {
    event.persist()
    this.setState({ [name]: event.target.value }, () => {
      this.updateCodeString()
    })
  }

  handleCheckbox = name => event => {
    this.setState({ [name]: event.target.checked })
  }

  handleItemClick = name => event => {
    let items = this.state[name]
    items.push({
      url: '',
    })

    this.setState(
      {
        [name]: items,
      },
      () => {
        this.updateCodeString()
      },
    )
  }

  render() {
    return (
      <div className="content">
        <Helmet title={this.state.helmetTitle}>
          
          <meta property="og:title" content={this.state.helmetTitle}/>
          <meta property="og:description" content={this.state.helmetDescription + " - " + this.state.siteName}/>
            
            
          
          <meta property="og:site_name" content={this.state.siteName}/>
          <meta property="og:url" content={window.location.href}/>
          <meta property="twitter:title" content={this.state.helmentTitle} />
          <meta property="twitter:description" content={this.state.helmetDescription} />
        </Helmet>
        <Row gutter={8}>
          <Col md={14}>
            <Card>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Title"
                      type="text"
                      name="name"
                      id=""
                      value={this.state.name}
                      onChange={this.handleInput('name')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="Job / Title"
                      type="text"
                      name="jobTitle"
                      id=""
                      value={this.state.jobTitle}
                      onChange={this.handleInput('jobTitle')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="City"
                      type="text"
                      name="city"
                      id=""
                      value={this.state.city}
                      onChange={this.handleInput('city')}
                    />
                  </Form>
                </Col>
              </Row>
              <Row gutter={8} className="m-4">
                <Col md={12}>
                  <Form>
                    {/* <Label for="">Title</Label> */}
                    <TextField
                      placeholder=" "
                      fullWidth
                      variant="outlined"
                      label="State / Province / Region"
                      type="text"
                      name="stateProvinceRegion"
                      id=""
                      value={this.state.stateProvinceRegion}
                      onChange={this.handleInput('stateProvinceRegion')}
                    />
                  </Form>
                </Col>
              </Row>
            </Card>
          </Col>
          <Col md={10}>
            <Row gutter={16}>
              <Toolbar style={{ backgroundColor: 'white', marginBottom: '10px' }}>
                <Col md={4}>
                  <Button
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleOnCopyClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Copy
                  </Button>
                </Col>
                <Col md={5}>
                  <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Download
                  </Button>
                </Col>
                <Col md={5}>
                  {/* <Button
                    className="ma-3"
                    fullWidth
                    variant="contained"
                    onClick={() => this.handleDownloadFileClick()}
                    color="primary"
                    style={{ outline: 'none' }}
                  >
                    Email
                    
                  </Button> */}
                </Col>
              </Toolbar>
            </Row>
            <Row>
              <AceEditor
                style={{ width: ' 100%' }}
                mode="php"
                theme="vibrant_ink"
                name="editor"
                id="editor"
                ref="ace"
                readOnly
                wrapEnabled
                value={this.state.codeItem}
                editorProps={{ $blockScrolling: true }}
                onLoad={editor => {
                  editor.focus()
                  editor.getSession().setUseWrapMode(true)
                }}
              />
            </Row>
          </Col>
        </Row>
        <Snackbar
          key={'saveId'}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          open={this.state.isSnackbarOpen}
          autoHideDuration={3000}
          onClose={() =>
            this.setState({
              isSnackbarOpen: false,
            })
          }
          onExited={this.handleExited}
          variant={'success'}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
        >
          <SnackbarContent
            style={{
              backgroundColor: '#42a5f5',
            }}
            message={<span id="message-id">{'Copied to the clipboard'}</span>}
          />
        </Snackbar>
      </div>
    )
  }
}

export default Person
`
  })

  inject('snippetBlankFile', () => {
    return `
`
  })
}
