export const state = () => ({
  username: 'Stranger',
})

export const mutations = {
  updateUsername(state, payload) {
    state.username = payload
  },
}
